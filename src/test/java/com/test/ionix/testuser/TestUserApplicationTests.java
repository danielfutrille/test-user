package com.test.ionix.testuser;

import java.util.Random;

import com.test.ionix.testuser.model.dto.EncryptDTO;
import com.test.ionix.testuser.model.entity.User;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestUserApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestUserApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String testEmail;

	public void setTestEmail(final String testEmail) {
		this.testEmail = testEmail;
	}

	public String getTestEmail() {
		return this.testEmail;
	}

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetAllUsers() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/api/v1/users", HttpMethod.GET, entity,
				String.class);

		Assert.assertNotNull(response.getBody());
	}

	@Test
	public void testCreateUser() {
		User user = new User();
		Random random = new Random();
		setTestEmail("testing." + random.nextInt() + "@example.com");

		user.setEmail(getTestEmail());
		user.setName("Testing Name");
		user.setPhone("456-" + random.nextInt());
		user.setUsername("testinUserName");

		ResponseEntity<User> postResponse = restTemplate.postForEntity(getRootUrl() + "/api/v1/users", user,
				User.class);
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
	}

	@Test
	public void testGetUserByEmail() {
		String testEmail = getTestEmail();
		User user = restTemplate.getForObject(getRootUrl() + "/api/v1/users-by-email?email=" + testEmail, User.class);
		System.out.println(user.getName());
		Assert.assertNotNull(user);
	}

	@Test
	public void testSearchIonix() {
		EncryptDTO searchResult = restTemplate.getForObject(getRootUrl() + "/api/v1/search?param=6", EncryptDTO.class);
		Assert.assertNotNull(searchResult);
		Assert.assertEquals("OK", searchResult.getDescription());
	}

}
