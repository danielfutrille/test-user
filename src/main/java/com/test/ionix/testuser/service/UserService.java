package com.test.ionix.testuser.service;

import java.util.ArrayList;
import java.util.List;

import com.test.ionix.testuser.exceptions.NotFoundException;
import com.test.ionix.testuser.model.entity.User;
import com.test.ionix.testuser.model.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    @Autowired
    UserRepository userRepository;

    public User create(User user) throws Exception {
        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new Exception("Error saving the User.");
        }
    }

    public User getByEmail(String email) throws Exception {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new NotFoundException("User with email = " + email + " not found."));

        return user;
    }

    public List<User> getAll() throws Exception {
        try {
            List<User> users = new ArrayList<User>();

            userRepository.findAll().forEach(users::add);

            return users;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
