package com.test.ionix.testuser.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.test.ionix.testuser.model.dto.EncryptDTO;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.web.client.RestTemplate;

@Service
public class EncryptService implements IEncryptService {

    /**
     * Path to the property of Ionix to make the endpoint test
     */
    @Value( "${app.ionix.endpoints.search}" )
    private String ionixEdnpointSearchUrl;

    /**
     * Path to the property of Ionix secretKey
     */
    @Value( "${app.ionix.secretKey}" )
    private String ionixSecretKey;
    
    /**
     * Get encrypted value from third-part endpoint
     */
    public EncryptDTO getEncrypString(final String param) throws Exception {

        StopWatch watch = new StopWatch();
        watch.start();

        String encryptedParam = encryptParam(param);
        RestTemplate restTemplate = new RestTemplate();
        EncryptDTO encryptDTO = restTemplate
                .getForObject(ionixEdnpointSearchUrl + encryptedParam, EncryptDTO.class);
        encryptDTO.getResult().setRegisterCount(encryptDTO.getResult().getItems().size());
        encryptDTO.getResult().setItems(null);

        watch.stop();
        encryptDTO.setElapsedTime(watch.getTotalTimeMillis());

        return encryptDTO;
    }

    /**
     * Encrypt the input param
     * 
     * @param param
     * @return
     * @throws Exception
     */
    private String encryptParam(final String param) throws Exception {
        String encryptedRut = "";
        try {
            DESKeySpec keySpec = new DESKeySpec(ionixSecretKey.getBytes("UTF8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, keyFactory.generateSecret(keySpec));
            byte[] encryptedBytes = cipher.doFinal(param.getBytes("UTF8"));
            byte[] encodedBytes = Base64.encodeBase64(encryptedBytes);

            encryptedRut = new String(encodedBytes);
        } catch (InvalidKeySpecException e) {
            throw new Exception(e.getMessage());
        } catch (InvalidKeyException e) {
            throw new Exception(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            throw new Exception(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(e.getMessage());
        } catch (NoSuchPaddingException e) {
            throw new Exception(e.getMessage());
        } catch (IllegalBlockSizeException e) {
            throw new Exception(e.getMessage());
        } catch (BadPaddingException e) {
            throw new Exception(e.getMessage());
        }

        return encryptedRut;
    }
}
