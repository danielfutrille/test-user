package com.test.ionix.testuser.service;

import java.util.List;

import com.test.ionix.testuser.model.entity.User;

public interface IUserService {

    public User create(final User user) throws Exception;

    public User getByEmail(final String email) throws Exception;

    public List<User> getAll() throws Exception;
}
