package com.test.ionix.testuser.service;

import com.test.ionix.testuser.model.dto.EncryptDTO;

public interface IEncryptService {

    public EncryptDTO getEncrypString(final String param) throws Exception;

}
