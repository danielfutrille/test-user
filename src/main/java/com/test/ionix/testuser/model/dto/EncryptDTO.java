package com.test.ionix.testuser.model.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EncryptDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6110858483412461363L;

    /**
     * Code of Response
     */
    private Integer responseCode;

    /**
     * Description of response
     */
    private String description;

    /**
     * Elapsed time of Response
     */
    private Long elapsedTime;

    /**
     * List os results
     */
    private ResultDTO result;

    /**
     * Complete constructor
     * 
     * @param responseCode
     * @param description
     * @param elapsedTime
     * @param result
     * @param registerCount
     */
    public EncryptDTO(final Integer responseCode, final String description, final Long elapsedTime,
            final ResultDTO result) {

        this.responseCode = responseCode;
        this.description = description;
        this.elapsedTime = elapsedTime;
        this.result = result;
    }

}
