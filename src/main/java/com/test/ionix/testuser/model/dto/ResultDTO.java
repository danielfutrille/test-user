package com.test.ionix.testuser.model.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ResultDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6110858483412461363L;

    /**
     * Code of Response
     */
    private List<Object> items;

    /**
     * Number of items
     */
    private Integer registerCount;
}
