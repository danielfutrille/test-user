package com.test.ionix.testuser.model.repository;

import java.util.Optional;

import com.test.ionix.testuser.model.entity.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findByEmail(final String email);
}