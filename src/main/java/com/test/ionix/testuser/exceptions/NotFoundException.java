package com.test.ionix.testuser.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author dfutrille
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    private final String textMessage;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public NotFoundException(final String message) {
        super(message);
        this.textMessage = message;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public String setTextMessage(final String textMessage) {
        return textMessage;
    }

    @Override
    public String toString() {
        return getLocalizedMessage();
    }

}
