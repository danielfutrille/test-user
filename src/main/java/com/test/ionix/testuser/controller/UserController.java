package com.test.ionix.testuser.controller;

import java.util.List;

import com.test.ionix.testuser.service.UserService;
import com.test.ionix.testuser.model.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String testService() {
        return "Service is running successfully";
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("users")
    /**
     * Create a new User
     * 
     * @param user
     * @return
     */
    public ResponseEntity<User> create(@RequestBody User user) {
        try {
            User savedItem = userService.create(user);
            return new ResponseEntity<>(savedItem, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    @GetMapping("users-by-email")
    /**
     * Get an User by email.
     * 
     * @param email
     * @return
     */
    public ResponseEntity<User> getByEmail(@RequestParam("email") String email) throws Exception {

        User user = userService.getByEmail(email);
        if (null != user) {
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("users")
    /**
     * List all registered Users
     * 
     * @return
     */
    public ResponseEntity<List<User>> getAll() {
        try {
            List<User> users = userService.getAll();

            if (users.isEmpty()) {
                return new ResponseEntity<>(users, HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}