package com.test.ionix.testuser.controller;

import com.test.ionix.testuser.service.EncryptService;
import com.test.ionix.testuser.model.dto.EncryptDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
class EncryptController {

    @Autowired
    EncryptService encryptService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("search")
    public ResponseEntity<EncryptDTO> getEncryptedString(@RequestParam(name = "param") String param) throws Exception {

        try {
            EncryptDTO value = encryptService.getEncrypString(param);
            return new ResponseEntity<>(value, HttpStatus.OK);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

}