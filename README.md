# Test User Project

Testing Project in Spring Boot: User Simple CRUD and Third-part endpoint testing with Encrypt support and Unit Tests

## Pre Requisites
+ Java JDK 11.0.8
+ MySQL 5.7.32
+ Apache Maven 3.6.0
+ Clone the repo from here: <https://gitlab.com/danielfutrille/test-user.git>


## Config Database
+ Open `src/main/resources/application.properties` and edit your database connection parameters.

Or you could have this database config:
+ An existing database named: `ionix`
+ Username: `ionix`
+ Password: `TestIonix.1`


## Build and start the application

Execute from the root path:
```bash
mvn clean install
mvn spring-boot:run
```

Now you can test the application running at: <http://localhost:8085/api/v1/> and you should get this message: `Service is running successfully`.


## REST Client
+ Postman (Recommended).
+ Download and Import this Collection: <https://gitlab.com/danielfutrille/test-user/-/blob/master/postman-collection.json>
+ Download and Import this Environment: <https://gitlab.com/danielfutrille/test-user/-/blob/master/postman-environment.json>


## Explore REST API
You can test the following endpoints (More info in Postman Collection).
+ Create and User: `POST /api/v1/users` with this JSON Body:
```json
{
  "name": "Testing Full Name",
  "username": "testingUserName",
  "email": "testingEmail@example.com",
  "phone": "+58-123-4567890"
}
```

+ Create and User: `GET /api/v1/users-by-email?email={email}`

+ Create and User: `GET /api/v1/users`

+ Search using thir-part endpoint with encrypted param: `GET /api/v1/search?param={param}` (Integer 1-9 param)



